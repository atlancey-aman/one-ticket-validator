package com.example.com.one_ticketchecker;

/**
 * Created by Navin Stark on 8/25/2018.
 */

public class ValidationInformation {

    public String name;
    public String commuteEnd;
    public String commuteStart;
    public String status;

    public ValidationInformation(String name, String commuteStart, String commuteEnd, String status){
        this.name = name;
        this.commuteStart = commuteStart;
        this.commuteEnd = commuteEnd;
        this.status = status;
    }

}
