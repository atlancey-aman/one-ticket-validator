package com.example.com.one_ticketchecker;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

public class Splash extends AppCompatActivity {
    Intent i;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_splash);
        i =new Intent(this,MainActivity.class);
        Thread t = new Thread() {
            public void run() {
                try
                {
                    Thread.sleep(2000);
                    startActivity(i);
                }
                catch (InterruptedException e)
                {
                    startActivity(i);
                }
            }
        };
        t.start();
    }
}
