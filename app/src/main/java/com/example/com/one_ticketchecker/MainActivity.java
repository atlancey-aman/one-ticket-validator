package com.example.com.one_ticketchecker;
//uosghcudicuodcohcbiohdc
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    Button bus,metro,train;
    String r;
    final Activity activity = this;
    Intent i;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bus=findViewById(R.id.Bus);
        train=findViewById(R.id.Train);
        metro=findViewById(R.id.Metro);
        bus.setOnClickListener(this);
        train.setOnClickListener(this);
        metro.setOnClickListener(this);


    }

    @Override
    public void onClick(View v)
    {i=new Intent(this,TicketInfo.class);

        if(v==bus)
        {
            i.putExtra("type","Bus");
            IntentIntegrator integrator = new IntentIntegrator(activity);
            integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE);
            integrator.setPrompt("Scan");
            integrator.setCameraId(0);
            integrator.setBeepEnabled(true);
            integrator.setBarcodeImageEnabled(false);
            integrator.initiateScan();

            //i.putExtra("message",r);
        }

        if(v==metro)
        {
            i.putExtra("type","Metro");

            IntentIntegrator integrator = new IntentIntegrator(activity);
            integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE);
            integrator.setPrompt("Scan");
            integrator.setCameraId(0);
            integrator.setBeepEnabled(true);
            integrator.setBarcodeImageEnabled(false);
            integrator.initiateScan();
           // i.putExtra("message",r);
        }

        if(v==train)
        {
            i.putExtra("type","Train");

            IntentIntegrator integrator = new IntentIntegrator(activity);
            integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE);
            integrator.setPrompt("Scan");
            integrator.setCameraId(0);
            integrator.setBeepEnabled(true);
            integrator.setBarcodeImageEnabled(false);
            integrator.initiateScan();
            //i.putExtra("message",r);
        }

       // startActivity(i);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(result != null){
            if(result.getContents()==null){
              Toast.makeText(this, "You cancelled the scanning", Toast.LENGTH_LONG).show();

            }
            else {
              //  Toast.makeText(this, result.getContents(),Toast.LENGTH_LONG).show();
                r=result.getContents();
                i.putExtra("me",r);
                startActivity(i);
            }
        }
        else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}
