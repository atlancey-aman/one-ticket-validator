package com.example.com.one_ticketchecker;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;


public class TicketInfo extends AppCompatActivity {
    String type,message;
    int condition;
    public static final String LOG_TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);



        setContentView(R.layout.activity_ticket_info);
        Intent rec=getIntent();
        Bundle b=rec.getExtras();
        type=b.getString("type");
        message=b.getString("me");
        ValidationAsyncTask task = new ValidationAsyncTask();
        task.execute();




    }
    //change

    private void updateUI(ValidationInformation validationInformation){
        LinearLayout background = (LinearLayout)findViewById(R.id.background);
        TextView validationText = (TextView)findViewById(R.id.validation);
        if(validationInformation.status == "valid") {
            validationText.setText("Ticket Validated");
            background.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.blue));

        }
        else{
            validationText.setText("Invalid Ticket");
            background.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.red));
        }

        TextView name = (TextView)findViewById(R.id.name);
        name.setText(validationInformation.name);

        TextView start = (TextView)findViewById(R.id.commute_start);
        start.setText(validationInformation.commuteStart);

        TextView end = (TextView)findViewById(R.id.commute_start);
        end.setText(validationInformation.commuteEnd);


    }

    private class ValidationAsyncTask extends AsyncTask<URL, Void, ValidationInformation >{
        @Override
        protected ValidationInformation doInBackground(URL... urls) {
            String jsonResponse = "{\n" +
                    "  \"name\": \"Aman Sharma\",\n" +
                    "  \"commute_end\": \"end\",\n" +
                    "  \"commute_start\": \"start\",\n" +
                    "  \"status\": \"valid/invalid\"\n" +
                    "}\n" +
                    "\n" +
                    "\n" +
                    "\n" +
                    "Collapse \n" +
                    "\n";

            ValidationInformation validData = extractFeatureFromJson(jsonResponse);
            return validData;
        }
        @Override
        protected void onPostExecute(ValidationInformation validationInformation) {
            if (validationInformation == null) {
                return;
            }

            updateUI(validationInformation);
        }

        private ValidationInformation extractFeatureFromJson(String validationJSON){
            if(TextUtils.isEmpty(validationJSON)){
                return null;
            }
            try {
                JSONObject baseJsonResponse = new JSONObject(validationJSON);
                String name = baseJsonResponse.getString("name");
                String commuteStart = baseJsonResponse.getString("commute_start");
                String commuteEnd = baseJsonResponse.getString("commute_end");
                String status = baseJsonResponse.getString("status");

                return new ValidationInformation(name,commuteStart,commuteEnd,status);
            }catch (JSONException e) {
                Log.e(LOG_TAG, "Problem parsing the earthquake JSON results", e);
            }
            return null;
        }


    }




}
